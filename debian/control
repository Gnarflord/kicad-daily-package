Source: kicad
Section: electronics
Priority: optional
Maintainer: Jean-Samuel Reynaud <js.reynaud@gmail.com>
Build-Depends: debhelper (>= 6), cmake (>= 2.6.0), doxygen, libbz2-dev, libcairo2-dev, libglu1-mesa-dev,
 libgl1-mesa-dev, libglew-dev, libx11-dev, libwxbase3.0-dev, libwxgtk3.0-gtk3-dev | libwxgtk3.0-dev,
 mesa-common-dev, pkg-config, libssl-dev, build-essential, cmake-curses-gui, debhelper, grep,
 python-dev | python3-dev, swig3.0, dblatex, po4a, asciidoc,
 python3-wxgtk4.0 | python-wxgtk3.0-dev,
 source-highlight, libboost-all-dev, libglm-dev (>= 0.9.5.1) | libglm-kicad-dev, libcurl4-openssl-dev,
 libgtk-3-dev, libngspice-kicad | libngspice0-dev, libngspice-kicad | ngspice-dev,
 libocct-modeling-algorithms-dev,
 libocct-modeling-data-dev,
 libocct-data-exchange-dev,
 libocct-visualization-dev,
 libocct-foundation-dev,
 libocct-ocaf-dev,
 zlib1g-dev, shared-mime-info (>= 2.0) | kicad-its-files
Standards-Version: 3.9.3
Homepage: http://www.kicad-pcb.org

Package: kicad
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${dist:Depends}, libngspice-kicad | libngspice0, xsltproc,
    libocct-modeling-algorithms-7.3 | libocct-modeling-algorithms-7.4 | libocct-modeling-algorithms-7.5,
    libocct-modeling-data-7.3 | libocct-modeling-data-7.4 | libocct-modeling-data-7.5,
    libocct-data-exchange-7.3 | libocct-data-exchange-7.4 | libocct-data-exchange-7.5,
    libocct-visualization-7.3 | libocct-visualization-7.4 | libocct-visualization-7.5,
    libocct-foundation-7.3 | libocct-foundation-7.4 | libocct-foundation-7.5,
    libocct-ocaf-7.3 | libocct-ocaf-7.4 | libocct-ocaf-7.5
Conflicts: kicad-common, kicad-locale-ko, kicad-locale-ru, kicad-locale-pl, kicad-locale-pt, kicad-locale-ja, kicad-locale-id,
    kicad-locale-ca, kicad-locale-it, kicad-locale-el, kicad-locale-zh, kicad-locale-de, kicad-locale-sl,
    kicad-locale-cs, kicad-locale-bg, kicad-locale-sv, kicad-locale-lt, kicad-locale-fi, kicad-locale-fr,
    kicad-locale-hu, kicad-locale-nl, kicad-locale-es, kicad-locale-sk
Recommends: kicad-libraries, kicad-doc-en, kicad-demos
Replaces: kicad-common
Suggests: extra-xdg-menus,
	kicad-doc-en | kicad-doc-fr | kicad-doc-ja | kicad-doc-it | kicad-doc-nl | kicad-doc-pl
Description: Electronic schematic and PCB design software
 KiCad is a suite of programs for the creation of printed circuit boards.
 It includes a schematic editor, a PCB layout tool, support tools and a
 3D viewer to display a finished & fully populated PCB.
 .
 KiCad is made up of 5 main components:
 .
  * kicad - project manager
  * eeschema - schematic editor
  * pcbnew - PCB editor
  * gerbview - GERBER viewer
  * cvpcb - footprint selector for components
 .
 Libraries:
  * Both eeschema and pcbnew have library managers and editors for their
    components and footprints
  * You can easily create, edit, delete and exchange library items
  * Documentation files can be associated with components, footprints and key
    words, allowing a fast search by function
  * Very large libraries are available for schematic components and footprints
  * Most components have corresponding 3D models


Package: kicad-dbg
Architecture: any
Depends: kicad (= ${binary:Version}), ${misc:Depends}
Description: Debug symbols for kicad
 KiCad is a suite of programs for the creation of printed circuit boards.
 It includes a schematic editor, a PCB layout tool, support tools and a
 3D viewer to display a finished & fully populated PCB.
 .
 KiCad is made up of 5 main components:
 .
  * kicad - project manager
  * eeschema - schematic editor
  * pcbnew - PCB editor
  * gerbview - GERBER viewer
  * cvpcb - footprint selector for components
 .
 Libraries:
  * Both eeschema and pcbnew have library managers and editors for their
    components and footprints
  * You can easily create, edit, delete and exchange library items
  * Documentation files can be associated with components, footprints and key
    words, allowing a fast search by function
  * Very large libraries are available for schematic components and footprints
  * Most components have corresponding 3D models


Package: kicad-demos
Architecture: all
Depends: ${misc:Depends}
Recommends: kicad
Replaces: kicad-demo
Conflicts: kicad-demo
Breaks: kicad-demo
Description: Common files used by kicad
 This package contains the component libraries and language files for KiCad.
